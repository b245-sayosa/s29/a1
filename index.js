// console.log("S29 Activity")
/*1
find users with letters S and d
use or operator
show only the first name and*/

//using or operator, regex and exclusion
db.users.find({
   $or:[
           {firstName: {$regex: "S"}},
           {lastName: {$regex: "D"}}
       ]},
    {
        _id: 0,
        contact: 0,
        department: 0,
        courses: 0,
        age: 0
        
    }
);




/*2
Find users who are from HR
age = 70
use $and operator
*/

//using and opertor and $gte
db.users.find({
    $and:[
            {department: "HR"},
            { age: { $gte:70 } }
        ]
});



/*3
Find users with letter e in firstName 

use and regex and lte operator*/

//using case sensitive regex and $lte

db.users.find({
    $and: [
            {firstName: {$regex: "e" , $options: "$i"}},
            {age: { $lte:30 }}
        ]
});